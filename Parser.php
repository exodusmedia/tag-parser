<?php
/**
 * Tag parsing library
 * @author Guillaume VanderEst <guillaume@vanderest.org>
 */
// TODO: implement escaping of characters
namespace Exo\Tag;
class Parser
{
	public $input;

	const TAG_OPENING_SYMBOL = '<';
	const TAG_OPENING_ENTITY = '&lt;';
	const TAG_CLOSING_SYMBOL = '>';
	const TAG_CLOSING_ENTITY = '&gt;';
	const ONE_SIDED_TAG_SYMBOL = '/';
	const NEGATIVE_TAG_SYMBOL = '/';

	const VALID_TAG_NAME_CHARACTERS = 'abcdefghijklmnopqrstuvwxyz_:';
	const VALID_ATTRIBUTE_NAME_CHARACTERS = 'abcdefghijklmnopqrstuvwxyz_-';

	const AFTER_TAG_NAME_CHARACTER = ' ';
	const AFTER_ATTRIBUTE_NAME_CHARACTER = '=';

	const VALID_VALUE_QUOTE_SYMBOLS = '\'"';
	const ESCAPE_SYMBOL = "\\";

	const COMMENT_STARTING_CHARACTER = '!';


	public function __construct($input)
	{
		$this->input = $input;
	}

	public function parse($input = NULL)
	{
		if ($input === NULL)
		{
			$input = $this->input;
		}

		$tags = array();
		if (empty($input)) { return $tags; }

		$total_length = strlen($input);

		// current state of parser
		$state = 'tag';

		$last_position = 0;
		$next_symbol = strpos($input, self::TAG_OPENING_SYMBOL);
		$next_entity = strpos($input, self::TAG_OPENING_ENTITY);

		// find the next available symbol position
		$next_position = FALSE;
		foreach (array($next_symbol, $next_entity) as $next)
		{
			if ($next_position === FALSE || $next < $next_position)
			{
				$next_position = $next;
			}
		}

		while ($next_position !== FALSE)
		{
			// current position
			$start_position = $current_position = $next_position;
			if ($current_position >= $total_length) { break; }
			$current_char = substr($input, $current_position, 1);

			// just started a new tag.. here's some setup
			$state = 'open';
			$tag_name = NULL;
			$tag_content = NULL;
			$attributes = array();
			$attribute_name = NULL;
			$attribute_value = NULL;
			$value_quoted_with = NULL;
			$tag_closing_name = NULL;

			$tag_valid = TRUE;
			$tag_complete = FALSE;

			$tag_starts_with = NULL;
			$closing_tag_starts_with = NULL;

			// tag created
			$tag = (object)array(
				'name' => $tag_name,
				'_content' => $tag_content,
				'_string' => $tag_string
			);

			// if it starts with a "<" or "&gt;" then continue as normal
			if (substr($input, $current_position, strlen(self::TAG_OPENING_ENTITY)) == self::TAG_OPENING_ENTITY)
			{
				$tag_starts_with = self::TAG_OPENING_ENTITY;
				$current_position += strlen(self::TAG_OPENING_ENTITY);
			} else {
				$tag_starts_with = $current_char;
				$current_position++;
			}

			// start parsing
			while (!$tag_complete && $tag_valid && $current_position < $total_length)
			{
				$current_char = substr($input, $current_position, 1);

				// FIXME: DELETE THIS LINE
				$DEBUG = substr($input, $start_position, $current_position - $start_position);

				// find the current state
				switch ($state)
				{
				case 'attribute':
					if (stripos(self::VALID_ATTRIBUTE_NAME_CHARACTERS, $current_char) !== FALSE)
					{
						$attribute_name .= $current_char;
					} elseif ($current_char == self::ONE_SIDED_TAG_SYMBOL) {
						$current_position++;
						$current_char = substr($input, $current_position, 1);
						if ($current_char == self::TAG_CLOSING_SYMBOL)
						{
							$tag_complete = TRUE;
						} elseif (substr($input, $current_position, strlen(self::TAG_CLOSING_ENTITY)) == self::TAG_CLOSING_ENTITY) {
							$current_position += strlen(self::TAG_CLOSING_ENTITY);
							$tag_complete = TRUE;
						}
					} elseif ($current_char == self::AFTER_ATTRIBUTE_NAME_CHARACTER) {
						$current_position++;
						$current_char = substr($input, $current_position, 1);
						if (strpos(self::VALID_VALUE_QUOTE_SYMBOLS, $current_char) !== FALSE)
						{
							$value_quoted_with = $current_char;
							$state = 'value';
						} else {
							throw new \Exception(sprintf('Invalid quote character in position %d: found "%s", expected "%s"', 
								$current_position,
								$current_char, 
								self::VALID_VALUE_QUOTE_SYMBOLS
							));
						}
					} else {
						throw new \Exception(sprintf('Invalid attribute name character in position %d: found "%s", expected "%s"', 
							$current_position,
							$current_char, 
							self::AFTER_ATTRIBUTE_NAME_CHARACTER
						));
					}
					break;
				case 'value':
					if ($current_char == self::ESCAPE_SYMBOL)
					{
						$current_position++;
						$current_char = substr($input, $current_position, 1);
						$attribute_value .= $current_char;
						$current_position++;
					} elseif ($current_char == $value_quoted_with) {
						$current_position++;
						$tag->$attribute_name = $attribute_value;
						$attribute_name = NULL;
						$attribute_value = NULL;
						$state = 'attribute';
					} else {
						$attribute_value .= $current_char;
					}
					break;
				case 'close':
					if (stripos(self::VALID_TAG_NAME_CHARACTERS, $current_char) !== FALSE) {
						$tag_closing_name .= $current_char;
					} elseif ($current_char == self::TAG_CLOSING_SYMBOL) {
						// not the matching closing tag of the opening tag
						if ($tag_closing_name !== $tag_name)
						{
							$tag_content .= $tag_closing_name;
							$state = 'content';
							$tag_closing_name = NULL;
						} else {
							$tag_complete = TRUE;
						}
					} elseif (substr($input, $current_position, strlen(self::TAG_CLOSING_ENTITY)) == self::TAG_CLOSING_ENTITY) {
						$current_position += strlen(self::TAG_CLOSING_ENTITY);
						// not the matching closing tag of the opening tag
						if ($tag_closing_name !== $tag_name)
						{
							$tag_content .= $tag_closing_name;
							$state = 'content';
							$tag_closing_name = NULL;
						} else {
							$tag_complete = TRUE;
						}
					} else {
						throw new \Exception(sprintf('Invalid closing tag character in position %d: found "%s", expected "%s" or "%s"', 
							$current_position,
							$current_char, 
							self::VALID_TAG_NAME_CHARACTERS,
							self::TAG_CLOSING_SYMBOL
						));
					}
					break;
				case 'content':
					if (substr($input, $current_position, strlen(self::TAG_OPENING_SYMBOL) + strlen(self::NEGATIVE_TAG_SYMBOL)) == (self::TAG_OPENING_SYMBOL . self::NEGATIVE_TAG_SYMBOL))
					{
						$state = 'close';
						$current_position += strlen(self::TAG_OPENING_SYMBOL) + strlen(self::NEGATIVE_TAG_SYMBOL) - 1;
						$tag->_content = $tag_content;
					} elseif (substr($input, $current_position, strlen(self::TAG_OPENING_ENTITY) + strlen(self::NEGATIVE_TAG_SYMBOL)) == (self::TAG_OPENING_ENTITY . self::NEGATIVE_TAG_SYMBOL))
					{
						$state = 'close';
						$current_position += strlen(self::TAG_OPENING_ENTITY) + strlen(self::NEGATIVE_TAG_SYMBOL) - 1;
						$tag->_content = $tag_content;
					} else {
						$tag_content .= $current_char;
					}
					break;
				case 'open':
					if (empty($tag_name) && $current_char == self::NEGATIVE_TAG_SYMBOL)
					{
						$tag_valid = FALSE;
					} elseif (substr($input, $current_position, strlen(self::ONE_SIDED_TAG_SYMBOL) + strlen(self::TAG_CLOSING_SYMBOL)) == (self::ONE_SIDED_TAG_SYMBOL . self::TAG_CLOSING_SYMBOL))
					{
						$current_position += strlen(self::ONE_SIDED_TAG_SYMBOL) + strlen(self::TAG_CLOSING_SYMBOL);
						$tag_complete = TRUE;
					} elseif (substr($input, $current_position, strlen(self::ONE_SIDED_TAG_SYMBOL) + strlen(self::TAG_CLOSING_ENTITY)) == (self::ONE_SIDED_TAG_SYMBOL . self::TAG_CLOSING_ENTITY))
					{
						$current_position += strlen(self::ONE_SIDED_TAG_SYMBOL) + strlen(self::TAG_CLOSING_SYMBOL);
						$tag_complete = TRUE;
					} elseif (stripos(self::VALID_TAG_NAME_CHARACTERS, $current_char) !== FALSE) {
						$tag_name .= $current_char;
					} elseif ($current_char == self::TAG_CLOSING_SYMBOL) {
						$tag->name = $tag_name;
						$state = 'content';
					} elseif (substr($input, $current_position, strlen(self::TAG_CLOSING_ENTITY)) == self::TAG_CLOSING_ENTITY) {
						$tag->name = $tag_name;
						$current_position += strlen(self::TAG_CLOSING_ENTITY);
						$state = 'content';
					} elseif ($current_char == self::AFTER_TAG_NAME_CHARACTER) {
						$state = 'attribute';
						$tag->name = $tag_name;
					} elseif ($current_char == self::COMMENT_STARTING_CHARACTER) {
						$tag_valid = FALSE;
					} else {
						throw new \Exception(sprintf('Invalid tag name character in position %d: found "%s", expected "%s"', 
							$current_position,
							$current_char, 
							self::VALID_TAG_NAME_CHARACTERS
						));
					}
					break;
				default:
					throw new \Exception(sprintf('Invalid state "%s"',
						$state
					));
				}

				$tag_length++;
				$current_position++;
			}

			if ($tag_complete)
			{
				$tag->_string = substr($input, $start_position, $current_position - $start_position);
				$tag->_offset = $start_position;
				$tag->_length = $current_position - $start_position;

				// detect whether this tag should be parseable, or if it's within another tag's content area-- and should be treated as plaintext
				$tag_addable = TRUE;
				foreach ($tags as $existing)
				{
					if ($tag->_offset >= $existing->_offset && ($tag->_offset + $tag->_length) <= ($existing->_offset + $existing->_length))
					{
						$tag_addable = FALSE;
					}
				}
				if ($tag_addable) { $tags[] = $tag; }
			}

			// start moving forward
			$last_position = $start_position + 1;

			$next_symbol = strpos($input, self::TAG_OPENING_SYMBOL, $last_position);
			$next_entity = strpos($input, self::TAG_OPENING_ENTITY, $last_position);
			$next_position = min($next_symbol, $next_entity);
		} 

		return $tags;
	}
}
